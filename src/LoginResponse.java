import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "loginResponse")
public class LoginResponse {

    private String code;

    private String ldSessionId;

    private String pdv;

    public LoginResponse() {
    }

    public LoginResponse(String code, String ldSessionId, String pdv) {
        this.code = code;
        this.ldSessionId = ldSessionId;
        this.pdv = pdv;
    }

    @XmlElement
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement(name = "LDSessionId")
    public String getLdSessionId() {
        return ldSessionId;
    }

    public void setLdSessionId(String ldSessionId) {
        this.ldSessionId = ldSessionId;
    }

    @XmlElement
    public String getPdv() {
        return pdv;
    }

    public void setPdv(String pdv) {
        this.pdv = pdv;
    }
}

