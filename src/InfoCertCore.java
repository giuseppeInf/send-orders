import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class InfoCertCore { //funziona

    // Valores de las propiedades:				//conservazionecl   INFLDC01   infocert1
    public static String INFOCERT_URL;// = "https://conservazione.infocert.it/ws/";// PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, "INFOCERT"));
    public static String DOCUMENT_URL = "document";// PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, "DOCUMENT");
    public static String SESSION = "session";// PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, "SESSION");
    public static String LOGIN_USER = "KAQND101";//PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, "LOGIN.USER"); NOT USED
    public static String LOGIN_PASS = "lDtw5Ypci";// PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, "LOGIN.PASS"); NOT USED

    public static HttpResponse callWS(String url, String method, HttpEntity entity) {
        return callWS(url, method, null, null, null, null, entity);
    }

    public static HttpResponse callWS(String url, String method, String mediaType, String acceptType, String ldSessionIdParamName, String ldSessionId, HttpEntity entity) {
    	INFOCERT_URL = MainWindow.getUrl();
    	
        url = INFOCERT_URL + url;
        System.out.println("URL: " + url);

        try {
            HttpRequestBase baseRequest = null;
            switch (method) {
                case HttpMethod.GET:
                    baseRequest = new HttpGet(url);
                    break;
                case HttpMethod.POST:
                    baseRequest = new HttpPost(url);
                    if (entity != null)
                        ((HttpPost) baseRequest).setEntity(entity);
                    break;
                case HttpMethod.PUT:
                    baseRequest = new HttpPut(url);
                    if (entity != null)
                        ((HttpPut) baseRequest).setEntity(entity);
                    break;
                case HttpMethod.DELETE:
                    baseRequest = new HttpDelete(url);
                    break;
            }
            if (mediaType != null) baseRequest.setHeader("Content-Type", mediaType);
            if (acceptType != null) baseRequest.setHeader("Accept", acceptType);
            if (ldSessionIdParamName != null) baseRequest.addHeader(ldSessionIdParamName, ldSessionId);
            else baseRequest.addHeader("ldSessionId", ldSessionId);
            //da settare per l'esibizione multipart
            //baseRequest.addHeader(â€œAcceptâ€�, â€œmultipart/mixedâ€�);
            HttpClient httpclient = new DefaultHttpClient();
            return httpclient.execute(baseRequest);
        } catch (Exception ex) {//gestire eccezione}
        	ex.printStackTrace();
        }
        return null;
    }

    public static String login(String resourceUrl, String userid, String password) throws Exception {
        try {
            JAXBContext jc = JAXBContext.newInstance(LoginResponse.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            List<NameValuePair> nameValuePairs = new ArrayList<>(2);
            nameValuePairs.add(new BasicNameValuePair("userid", userid));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            HttpResponse response = callWS(resourceUrl, HttpMethod.POST, new UrlEncodedFormEntity(nameValuePairs));
            HttpEntity resEntity = response.getEntity();
            String resXML = EntityUtils.toString(resEntity);

            LoginResponse loginResponse = (LoginResponse) unmarshaller.unmarshal(new InputSource(new ByteArrayInputStream(resXML.getBytes("utf-8"))));

            return loginResponse.getLdSessionId();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String conserve(String conserveResourceUrl, String bucket, InputStream parametersStream, String parametersFileName, InputStream indexStream, String indexMimeType, String indexFileName,
                                    InputStream dataStream, String dataMimeType, String dataFileName, String sessionId) {
        try {
            // New Way:
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            ContentType ct = ContentType.create("text/xml;1.0");
            builder.addBinaryBody("PARAMFILE", parametersStream, ct, parametersFileName);
            builder.addBinaryBody("INDEXFILE", indexStream, ContentType.create("text/xml;1.0"), indexFileName);
            builder.addBinaryBody("DATAFILE", dataStream, ContentType.create("application/pdf;NA"), dataFileName);
            HttpEntity multipart = builder.build();

            System.out.println("Enviando pdf: " + dataFileName + ", paramXml: " + parametersFileName + ", indexXml: " + indexFileName + ", con bucket: " + bucket);

            // creazione request
            //HttpResponse response = callWS(conserveResourceUrl, HttpMethod.POST, requestEntity);   //PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, "LOGOUT.SESSION_ID")
            HttpResponse response = callWS(bucket + "/" + conserveResourceUrl, HttpMethod.POST, null, null, "LdsessionId", sessionId, multipart);
            String msg = "";
            if (response.getStatusLine().getStatusCode() == 201) {
                // chiamata conserve ok
                msg = "Documento inviato correttamente in conservazione" + EntityUtils.toString(response.getEntity());
                System.out.println( msg);
                indexStream.close();
                dataStream.close();
                parametersStream.close();
                return "1";
            } 
            else {
                msg = "Impossibile conservare il file per: " + EntityUtils.toString(response.getEntity());
                System.out.println( msg);
                indexStream.close();
                dataStream.close();
                parametersStream.close();
                return msg;
            }
        } catch (Exception ex) {//gestire eccezione
            ex.printStackTrace();
        }
        System.out.println( Response.Status.INTERNAL_SERVER_ERROR );
        return "0";
        //return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    public static Response retrieve(String retrieveResourceUrl, String bucket, String pdfToken, String sessionId) {

        Response response = null;

        try {
            //response = callWS(bucket + "/" + retrieveResourceUrl + "/" + pdfToken, HttpMethod.GET, null, null, null, sessionId, null);
            response = retrievePdf(bucket, pdfToken, sessionId);
            //if (response.getStatusLine().getStatusCode() == 200) System.out.println("Documento recuperado");
            //else System.err.println("No se ha podido recuperar el documento, razÃ³n: " + EntityUtils.toString(response.getEntity()));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Response.ok(response.getEntity()).build();
    }

    private static Response retrievePdf(String bucket, String pdfToken, String sessionId) {
        // Create the client for the request:
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(INFOCERT_URL).path(bucket).path("document").path(pdfToken);
        Invocation.Builder builder = target.request();

        Response response = builder.header("ldSessionId", sessionId).get();
        Response.ResponseBuilder responseBuilder = Response.ok(response.getEntity());
        responseBuilder.header("Content-Disposition", "attachment; filename=\""+ pdfToken + ".zip\"");

        System.out.println(response.getEntity().toString());

        client.close();

        return responseBuilder.build();
    }

    public static String logout(String sessionId) {
        HttpResponse response = callWS(SESSION, HttpMethod.DELETE, null, null, PropertiesLoader.loadProperty(PropertiesTypes.CONSTANTS, "LOGOUT.SESSION_ID"), sessionId, null);
        HttpEntity resEntity = response.getEntity();
        Response.ResponseBuilder res;

        String resString = "";
        try {
            resString = EntityUtils.toString(resEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resString;
    }
}
