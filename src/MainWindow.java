import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.ColorUIResource;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.print.Doc;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JComboBox;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.xml.sax.InputSource;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BadPdfFormatException;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.Point;
import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.TextArea;
import javax.swing.JTextPane;

public class MainWindow extends JFrame {

	private static final long serialVersionUID = -6499566697153285244L; // serializable

	private static final String NOT_FOUND = "";

	private final String ERRATE_VALUE = "Valore errato.";

	public static final String newline = System.getProperty("line.separator");

	private static String url; // url Infocert per invio in test o produzione
	private String user; // user Infocert per invio in conservazione
	private String password; // password Infocert per invio in conservazione
	private String sessionId; // sessionID Infocert per invio conservazione
	private String pdv; // PDV Infocert per invio in conservazione
	private String idBucket; // idBucket Infocert per invio in conservazione
	private String idPolicy; // idPolicy Infocert per invio in conservazione

	private JLabel labelOP;
	private JPanel panel;

	private int n;
	private File[] files;
	private int numFile;
	private int numFileCorretti;

	private Vector<String> vector;
	private JTextPane textPane;
	private StringBuffer stringBuffer;
	private JScrollPane scrollPane;

	private JOptionPane pane;
	private final JLabel lblNewLabel = new JLabel("SEND ORDERS TO INFOCERT");
	private JLabel fileCaricati;
	private UIManager manager;
	private final ButtonGroup buttonGroup = new ButtonGroup();;
	private JRadioButton infocertTest = new JRadioButton("Test");
	private JRadioButton infocertProCitroen = new JRadioButton("Citroen");
	private JRadioButton infocertProPeugeot = new JRadioButton("Peugeot");

	@SuppressWarnings("static-access")
	public MainWindow() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setForeground(Color.LIGHT_GRAY);
		manager = new UIManager();
		manager.put("OptionPane.messageFont", new Font("Helvetica", Font.BOLD, 13));
		manager.put("OptionPane.messageForeground", new ColorUIResource(102, 0, 51));

		this.setTitle("Send orders to Infocert - Local Application");
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// this.getContentPane().setBackground(new Color(204, 204, 204));

		JButton btnSelezionaFile = new JButton("Seleziona file");
		btnSelezionaFile.setBounds(42, 262, 130, 23);
		btnSelezionaFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selectFiles();
			}
		});

		this.getContentPane().setLayout(null);

		JLabel lblRenameFilesVersione = new JLabel(
				"Send orders to Infocert - Version 1.0 - 2018 \u00A9 GC - Grupo CMC. All rights reserved.");
		lblRenameFilesVersione.setBounds(10, 400, 782, 20);
		lblRenameFilesVersione.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblRenameFilesVersione.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblRenameFilesVersione);

		numFileCorretti = 0;

		files = new File[] {};
		numFile = files.length;

		lblNewLabel.setBounds(131, 0, 506, 31);
		lblNewLabel.setForeground(new Color(102, 0, 51));
		lblNewLabel.setFont(new Font("Algerian", Font.PLAIN, 26));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblNewLabel);

		this.setSize(810, 461);
		this.setLocation(330, 200);

		fileCaricati = new JLabel();
		fileCaricati.setBounds(646, 36, 158, 20);
		fileCaricati.setHorizontalAlignment(SwingConstants.LEFT);
		fileCaricati.setForeground(new Color(102, 0, 51));
		fileCaricati.setFont(new Font("Arial Black", Font.PLAIN, 15));
		fileCaricati.setText("File caricati:  " + files.length);
		getContentPane().add(fileCaricati);

		JLabel lblImgLogo = new JLabel(
				new ImageIcon(new File("img/Cognicase Managment Consulting SRL.png").getAbsolutePath()));
		lblImgLogo.setBounds(0, 326, 135, 61);
		getContentPane().add(lblImgLogo);

		JButton btnInviaInConservazione = new JButton("Invia");
		btnInviaInConservazione.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnInviaInConservazione.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int res = JOptionPane.showConfirmDialog(getComponent(0),
						"Sei sicuro di voler procedere all'invio di " + files.length
								+ ((files.length == 1) ? " ordine " : " ordini ")
								+ (infocertProCitroen.isSelected() ? "Citroen?" : "Peugeot?"),
						"Conferma invio", JOptionPane.INFORMATION_MESSAGE);
				if (res == 1 || res == 2) // res = 0 ->SI, res = 1 ->NO, res = 2 ->ANNULLA
					return;

				labelOP.setText("Processo di invio in conservazione in corso... Attendere prego...");
				labelOP.paintImmediately(labelOP.getVisibleRect());
				completeLog("**********INIZIO ELABORAZIONE - PROCESSO DI INVIO IN CONSERVAZIONE SOSTITUTIVA**********",
						"START SEND");
				send(); // invia in conservazione i file che sono stati rinominati correttamente
				completeLog("**********FINE ELABORAZIONE - PROCESSO DI INVIO IN CONSERVAZIONE SOSTITUTIVA**********\r",
						"END SEND");
				labelOP.setText("Nessuna operazione in corso...");
				labelOP.paintImmediately(labelOP.getVisibleRect());
			}
		});
		btnInviaInConservazione.setBounds(663, 302, 117, 31);
		getContentPane().add(btnInviaInConservazione);

		panel = new JPanel();
		panel.setBounds(133, 42, 504, 291);
		panel.setLayout(null);
		getContentPane().add(panel);
		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 482, 267);
		panel.add(scrollPane);

		textPane = new JTextPane();
		textPane.setEditable(false);
		scrollPane.setViewportView(textPane);
		labelOP = new JLabel("Nessuna operazione in corso...");
		labelOP.setHorizontalAlignment(SwingConstants.CENTER);
		labelOP.setFont(new Font("Tahoma", Font.BOLD, 14));
		labelOP.setBounds(133, 334, 504, 20);
		getContentPane().add(labelOP);

		buttonGroup.add(infocertTest);
		infocertTest.setFont(new Font("Tahoma", Font.PLAIN, 14));
		infocertTest.setBounds(10, 98, 97, 25);
	//	getContentPane().add(infocertTest);

		buttonGroup.add(infocertProCitroen);
		infocertProCitroen.setFont(new Font("Tahoma", Font.PLAIN, 14));
		infocertProCitroen.setBounds(10, 42, 97, 23);
		infocertProCitroen.setSelected(true);
		getContentPane().add(infocertProCitroen);

		buttonGroup.add(infocertProPeugeot);
		infocertProPeugeot.setFont(new Font("Tahoma", Font.PLAIN, 14));
		infocertProPeugeot.setBounds(10, 70, 97, 23);
		getContentPane().add(infocertProPeugeot);

		JButton btnSelezionaFile_1 = new JButton("Seleziona File");
		btnSelezionaFile_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selectFiles();
			}
		});
		btnSelezionaFile_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSelezionaFile_1.setBounds(663, 252, 117, 31);
		getContentPane().add(btnSelezionaFile_1);

	}

	public void selectFiles() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setMultiSelectionEnabled(true); // abilito la multiselezione
		fileChooser.setFileFilter(new FileNameExtensionFilter("*.pdf", "pdf", "PDF"));
		n = fileChooser.showOpenDialog(MainWindow.this);
		files = fileChooser.getSelectedFiles();
		numFile = files.length;
		fileCaricati.setText("File caricati:  " + files.length);
	}

	// stampa sul pannello principale dell'applicazione l'esito di ogni file
	// elaborato
	public void printOnTextPane(int type, int indice) {
		if (type == -1)
			vector.addElement("Processed: " + files[indice].getName() + " - [ERROR]\r");
		else if (type == 0)
			vector.addElement("Processed: " + files[indice].getName() + " - [SUCCESS]\r");
		else
			vector.addElement("Processed: " + files[indice].getName() + " - has " + type + " pages.\r");

		stringBuffer.append(vector.get(indice + 1));

		textPane.setText(stringBuffer.toString());
		textPane.paintImmediately(scrollPane.getVisibleRect());
	}

	public void setNumFileCorretti(int numFileCorretti) {
		this.numFileCorretti = numFileCorretti;
	}

	public int getNumFileCaricati() {
		return files.length;
	}

	public static String getUrl() {
		return url;
	}

	public boolean isDate(String dataDocumento) {

		try {
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			df.setLenient(false);
			df.parse(dataDocumento);

		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// Messaggi di errore di tipo JOptionPane
	@SuppressWarnings("static-access")
	public void errorMessage(Component c, String fileName) {
		String message;
		pane = new JOptionPane();

		if (!fileName.endsWith(".pdf")) {
			message = "Il file " + fileName + " non e' un pdf, carica un file corretto.";
			pane.showMessageDialog(c, message, "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			message = "Errore nella rinomina del file.";
			pane.showMessageDialog(c, message.concat(fileName), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	// Messaggi di warning di tipo JOptionPane
	@SuppressWarnings("static-access")
	public void warningMessage(Component c, int type) {
		String message;
		pane = new JOptionPane();

		if (type == 1)
			message = "Seleziona il tipo di fattura prima di procedere.";
		else if (type == 2)
			message = "Nessun file trovato nella cartella.";
		else
			message = null;

		pane.showMessageDialog(c, message, "Warning", JOptionPane.INFORMATION_MESSAGE);
	}

	// Messaggi di warning per metodo split di tipo JOptionPane
	@SuppressWarnings("static-access")
	public void warningMessageSplit(Component c, int numFile) {
		String message = new String();
		pane = new JOptionPane();

		if (numFile <= 0)
			message = "Nessun file caricato, caricarne uno e quindi riprovare.";
		else if (numFile > 1)
			message = "Non e' possibile caricare pi� file per effettuare lo split.\nCaricare soltanto un file.";

		pane.showMessageDialog(c, message, "Warning", JOptionPane.INFORMATION_MESSAGE);
	}

	// Messaggi di informazioni varie di tipo JOptionPane
	@SuppressWarnings("static-access")
	public void informationMessage(Component c, int lenght, int numFileCorretti, int type) {
		pane = new JOptionPane();
		if (type == 1)
			pane.showMessageDialog(c, "Caricati " + lenght + " file.", "Information", JOptionPane.INFORMATION_MESSAGE);
		else if (type == 2)
			pane.showMessageDialog(c,
					"Numero file caricati: " + lenght + ".\nNumero file rinominati correttamente: " + numFileCorretti
							+ "\nNumero file in errore " + (lenght - numFileCorretti) + ".",
					"Information", JOptionPane.INFORMATION_MESSAGE);
	}

	// log completo di tutto il processo
	public void completeLog(String fileName, String evento) {
		final String START_SEND = "START SEND";
		final String END_SEND = "END SEND";
		final String END = "END";
		final String TOTAL = "TOTAL";
		final String DO_NOTHING = "DO NOTHING";

		String file_name = "ProcessLog.txt";
		// String path = scegliereUnPathSeServira_file_name;
		String completeLogString;

		File processLog = new File(file_name);

		if (evento.equals(START_SEND) || evento.equals(END_SEND) || evento.equals(DO_NOTHING))
			completeLogString = "\n[" + LocalDateTime.now().getDayOfMonth() + "-" + LocalDateTime.now().getMonthValue()
					+ "-" + LocalDateTime.now().getYear() + " " + LocalDateTime.now().getHour() + ":"
					+ LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond() + "]" + " - " + fileName
					+ "\n";
		else if (fileName.equals(TOTAL) && evento.equals(END))
			completeLogString = "\n[" + LocalDateTime.now().getDayOfMonth() + "-" + LocalDateTime.now().getMonthValue()
					+ "-" + LocalDateTime.now().getYear() + " " + LocalDateTime.now().getHour() + ":"
					+ LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond() + "]"
					+ " - RIEPILOGO: \nFILE TOTALI ELABORATI: " + files.length + "\n | FILE RINOMINATI CORRETTAMENTE: "
					+ numFileCorretti + "\n | FILE IN ERRORE: " + (files.length - numFileCorretti) + "\n";
		else
			completeLogString = "\n[" + LocalDateTime.now().getDayOfMonth() + "-" + LocalDateTime.now().getMonthValue()
					+ "-" + LocalDateTime.now().getYear() + " " + LocalDateTime.now().getHour() + ":"
					+ LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond() + "]" + " - Il file "
					+ fileName + " " + evento + "\n";

		ArrayList<String> log = new ArrayList<>();
		log.add(completeLogString);
		if (processLog.exists()) {
			try {
				Files.write(Paths.get(file_name), log, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				Files.write(Paths.get(file_name), log, Charset.forName("UTF-8"), StandardOpenOption.CREATE_NEW);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// log che riguarda solo gli errori di invio in conservazione
	public void errorLogSend(String fileName, String cause) {
		String file_name = "errorLogInvio.txt";
		String path = file_name;// path_KO_sended.concat(file_name);
		String completeLogString;

		File logFile = new File(path);
		if (fileName == null)
			completeLogString = "\n[" + LocalDateTime.now().getDayOfMonth() + "-" + LocalDateTime.now().getMonthValue()
					+ "-" + LocalDateTime.now().getYear() + " " + LocalDateTime.now().getHour() + ":"
					+ LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond() + "]" + cause + "\n";
		else
			completeLogString = "\n[" + LocalDateTime.now().getDayOfMonth() + "-" + LocalDateTime.now().getMonthValue()
					+ "-" + LocalDateTime.now().getYear() + " " + LocalDateTime.now().getHour() + ":"
					+ LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond() + "]" + " - Il file "
					+ fileName + " non e' stato inviato in conservazione perch�" + cause + "\n";

		ArrayList<String> log = new ArrayList<>();
		log.add(completeLogString);
		if (logFile.exists()) {
			try {
				Files.write(Paths.get(path), log, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				Files.write(Paths.get(path), log, Charset.forName("UTF-8"), StandardOpenOption.CREATE_NEW);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// carica i parametri di Infocert
	public void loadPropertiesInfocert(int environment) {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);

			// get the property value and assign it to each variable
			if (environment == 0) { // test environment
				url = prop.getProperty("urlInfTest");
				user = prop.getProperty("userInfTest");
				password = prop.getProperty("passInfTest");
				idBucket = prop.getProperty("idBucketTest");
				idPolicy = prop.getProperty("idPolicyTest");
			} else if (environment == 1) { // Citroen production environment
				url = prop.getProperty("urlInfProCitr");
				user = prop.getProperty("userInfProCitr");
				password = prop.getProperty("passInfProCitr");
				idBucket = prop.getProperty("idBucketProCitr");
				idPolicy = prop.getProperty("idPolicyProCitr");
			} else if (environment == 2) { // Peugeot production environment
				url = prop.getProperty("urlInfProPeug");
				user = prop.getProperty("userInfProPeug");
				password = prop.getProperty("passInfProPeug");
				idBucket = prop.getProperty("idBucketProPeug");
				idPolicy = prop.getProperty("idPolicyProPeug");
			} else
				return;

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean isNumber(char lettera) {
		if (Character.isDigit(lettera)) {
			return true;
		} else
			return false;
	}

	/***************** INIZIO INVIO IN CONSERVAZIONE *****************/
	public void send() {
		int environment = setInfocertEnvironment();
		String[] esiti;
		String[] date;

		fileCaricati.setText("File caricati:  " + files.length);
		fileCaricati.paintImmediately(fileCaricati.getVisibleRect());

		if (files == null) {
			JOptionPane.showMessageDialog(getComponent(0), "Errore imprevisto, contattare un admin.",
					"Unexpected error", JOptionPane.ERROR_MESSAGE);
			completeLog("La cartella da cui prelevare i file non esiste.\n"
					+ "Non posso inviare i file se non esiste la cartella che li contiene.\n"
					+ "variabile files == null", "DO NOTHING");
			return;
		}
		if (files.length == 0) {
			JOptionPane.showMessageDialog(getComponent(0),
					"Nessun file da inviare. Caricarne qualcuno e quindi riprovare.", "Warning",
					JOptionPane.INFORMATION_MESSAGE);
			completeLog("Nessun file presente nella cartella, nulla da inviare.", "DO NOTHING");
			return;
		}

		try {

			loadPropertiesInfocert(environment);
			ArrayList<NameValuePair> loginParam = new ArrayList<NameValuePair>(2);
			loginParam.add(new BasicNameValuePair("userid", user));
			loginParam.add(new BasicNameValuePair("password", password));
			HttpResponse response = InfoCertCore.callWS("session", javax.ws.rs.HttpMethod.POST,
					new UrlEncodedFormEntity(loginParam));
			HttpEntity resEntity = response.getEntity();
			String resultXML = EntityUtils.toString(resEntity);

			JAXBContext jc = JAXBContext.newInstance(LoginResponse.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			LoginResponse loginResponse = (LoginResponse) unmarshaller
					.unmarshal(new InputSource(new ByteArrayInputStream(resultXML.getBytes("utf-8"))));
			if (loginResponse.getCode().equals("OK")) {
				sessionId = loginResponse.getLdSessionId();
				pdv = loginResponse.getPdv();
				System.out.println("Login avvenuto correttamente.");
				completeLog("Login avvenuto correttamente.", "DO NOTHING");
			} else {
				System.out.println("Login NON avvenuto correttamente.");
				completeLog("Login NON avvenuto correttamente.", "DO NOTHING");
				JOptionPane.showMessageDialog(getComponent(0),
						"Login errato, verificare credenziali e quindi riprovare.", "Warning",
						JOptionPane.INFORMATION_MESSAGE);
				errorLogSend(null, "Nessuno dei " + files.length
						+ " file presenti e' stato inviato in conservazione poiche' le credenziali di accesso sono errate.");
				return;
			}
		} catch (Exception ex) {
			System.out.println("Login Error");
			completeLog("LOGIN NON AVVENUTO CORRETTAMENTE.", "DO NOTHING");
			JOptionPane.showMessageDialog(getComponent(0), "Login errato, verificare credenziali e quindi riprovare.",
					"Warning", JOptionPane.INFORMATION_MESSAGE);
			errorLogSend(null, "Nessuno dei " + files.length
					+ " file presenti e' stato inviato in conservazione poiche' le credenziali di accesso sono errate.");

			return;
		}
		stringBuffer = new StringBuffer();
		vector = new Vector<String>();
		vector.addElement("START PROCESSO DI INVIO IN CONSERVAZIONE\r");
		stringBuffer.append(vector.firstElement());
		textPane.setText(stringBuffer.toString());
		textPane.paintImmediately(scrollPane.getVisibleRect());
		int fileSended = 0;
		esiti = new String[files.length];
		date = new String[files.length];
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date today;
		String reportDate;

		for (int i = 0; i < files.length; i++) {
			String fileNameToSend = files[i].getName();

			if (!fileNameToSend.endsWith(".pdf") && !fileNameToSend.endsWith(".PDF")) {
				JOptionPane.showMessageDialog(getComponent(0),
						"Il file " + fileNameToSend + " non e' un file pdf e quindi non e' stato elaborato.", "Warning",
						JOptionPane.WARNING_MESSAGE);
				completeLog("Il file " + fileNameToSend + " non e' un file pdf ed � quindi non e' stato elaborato.",
						"DO NOTHING");
				errorLogSend(null,
						"Il file " + fileNameToSend + " non e' un file pdf e quindi non e' stato elaborato.");
				printOnTextPane(-1, (i));
				esiti[i] = "NO";
				// Get the date
				today = Calendar.getInstance().getTime();
				// Using DateFormat format method we can create a string
				reportDate = df.format(today);
				date[i] = (reportDate);
				continue;
			}

			int yearCurrent = LocalDate.now().getYear();

			String yearCurrentToString = String.valueOf(yearCurrent);

			// Creation on index
			String path = "index.xml";
			// Delete file index.xml
			deleteFile(path);

			try {
				Files.write(Paths.get(path), getIndexXMLFatture(fileNameToSend, environment),
						Charset.forName("ISO-8859-1"), StandardOpenOption.CREATE);
				completeLog("Generazione del file index.xml avvenuta correttamente per il file " + files[i].getName(),
						"DO NOTHING");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				errorLogSend(fileNameToSend,
						" c'e' stato un problema durante la creazione del file index.xml, controllare il file.");
				completeLog("Errore durante la generazione del file index.xml per il file " + files[i].getName(),
						"DO NOTHING");
				System.out.println("Error generating XML for Parameters");
				e.printStackTrace();
			}

			String indexHash = "";
			File indexFile = new File(path);

			try {
				indexHash = getHash(indexFile);
				completeLog(
						"Generazione hash del file index.xml avvenuta correttamente per il file " + files[i].getName(),
						"DO NOTHING");
			} catch (Exception e) {
				errorLogSend(fileNameToSend,
						" c'e' stato un problema durante la generazione dell' hash per il file index.xml, controllare il file.");
				completeLog("Errore durante la generazione dell'hash di index.xml per il file " + files[i].getName(),
						"DO NOTHING");
				System.out.println("Error generating XML for Parameters");
				e.printStackTrace();
			}

			String pdfHash = "";
			try {
				pdfHash = getHash(files[i]);
				completeLog("Generazione hash avvenuta correttamente per il file " + files[i].getName(), "DO NOTHING");
			} catch (Exception e) {
				errorLogSend(fileNameToSend,
						" c'e' stato un problema durante la generazione dell' hash per il file " + fileNameToSend);
				completeLog("Errore durante la generazione dell'hash per il file " + files[i].getName(), "DO NOTHING");
				System.out.println("Error generating hash for index file");
				e.printStackTrace();
			}
			
			// Creation of parameter
			path = "conserve.xml";
			// Delete file index.xml
			deleteFile(path);

			try {
				Files.write(Paths.get(path),
						getParamsXMLFatture(yearCurrentToString, fileNameToSend, indexHash, pdfHash),
						Charset.forName("UTF-8"), StandardOpenOption.CREATE);
				completeLog(
						"Generazione del file conserve.xml avvenuta correttamente per il file " + files[i].getName(),
						"DO NOTHING");
			} catch (IOException e) {
				errorLogSend(fileNameToSend,
						" c'e' stato un problema durante la creazione del file conserve.xml, controllare il file.");
				System.out.println("Error generating XML for Parameters");
				completeLog("Errore durante la generazione del file conserve.xml.", "DO NOTHING");
				e.printStackTrace();
			}

			// Send to server
			String indexMimeType = "";
			String dataMimeType = "";
			InputStream parameterStream = null;

			try {
				parameterStream = new FileInputStream("conserve.xml");
			} catch (Exception e) {
				errorLogSend(fileNameToSend,
						" c'e' stato un problema durante la lettura del file conserve.xml, controllare il file.");
				completeLog("Errore durante la lettura del file conserve.xml per il file " + files[i].getName(),
						"DO NOTHING");
				e.printStackTrace();
				System.out.println("Conserve XML not Found");
			}

			InputStream indexStream = null;

			try {
				indexStream = new FileInputStream("index.xml");
			} catch (Exception e) {
				errorLogSend(fileNameToSend,
						" c'e' stato un problema durante la lettura del file index.xml, controllare il file.");
				completeLog("Errore durante la lettura del file index.xml per il file " + files[i].getName(),
						"DO NOTHING");
				e.printStackTrace();
				System.out.println("Index XML not Found.");
			}

			InputStream dataStream = null;
			try {
				dataStream = new FileInputStream(files[i]);
			} catch (Exception e) {
				e.printStackTrace();
				errorLogSend(fileNameToSend, " c'e' stato un problema durante la lettura del file " + fileNameToSend
						+ " controllare il file.");
				System.out.println("PDF File not Found");
			}

			System.out.println("SessionID: " + sessionId);

			completeLog("Tento di inviare il file " + files[i].getName() + " in conservazione sostitutiva...",
					"DO NOTHING");

			String res = InfoCertCore.conserve("document", this.idBucket, parameterStream, "conserve.xml", indexStream,
					indexMimeType, "index.xml", dataStream, dataMimeType, files[i].getName(), sessionId);

			if (res == "1") {
				System.out.println("Document uploaded");
				completeLog("Il file " + files[i].getName()
						+ " e' stato inviato correttamente in conservazione sostitutiva.", "DO NOTHING");
				printOnTextPane(0, i);
				fileSended++;
				esiti[i] = "SI";
				// Get the date
				today = Calendar.getInstance().getTime();
				// Using DateFormat format method we can create a string
				reportDate = df.format(today);
				date[i] = (reportDate);
			} else {
				printOnTextPane(-1, (i));
				completeLog("Il file " + files[i].getName() + " non e' stato inviato in conservazione sostitutiva.",
						"DO NOTHING");
				esiti[i] = "NO";
				// Get the date
				today = Calendar.getInstance().getTime();
				// Using DateFormat format method we can create a string
				reportDate = df.format(today);
				date[i] = (reportDate);
				try {
					errorLogSend(fileNameToSend, " \"" + res.substring(132, 192) + "\""
							+ ". Il servizio ha generato il seguente codice di errore: " + res.substring(104, 112)
							+ ". Questa la risposta del servizio: " + "\"" + res + "/");
				} catch (Exception ex) {
					ex.printStackTrace();
					errorLogSend(fileNameToSend, " \"" + "Questa la risposta del servizio: " + "\"" + res + "/");
				}
				System.out.println("Document not uploaded");
			}

			System.out.println("XML Creation END");
			// XML Creation end
		}
		vector.addElement("END PROCESSO DI INVIO IN CONSERVAZIONE\r");
		stringBuffer.append(vector.lastElement());
		textPane.setText(stringBuffer.toString());
		textPane.paintImmediately(scrollPane.getVisibleRect());
		JOptionPane.showMessageDialog(getComponent(0),
				"Numero totale file: " + files.length + "\nNumero file inviati correttamente in conservazione: "
						+ fileSended + "\nNumero file in errore: " + (files.length - fileSended),
				"Information", JOptionPane.INFORMATION_MESSAGE);
		completeLog("- RIEPILOGO SESSIONE: Totale file elaborati: " + files.length
				+ "\n | Totale file inviati correttamente: " + fileSended + "\n | Totale file in errore (NON inviati): "
				+ (files.length - fileSended) + "\n | Session id: " + sessionId + " \n | PDV: " + pdv, "DO NOTHING");

		ExcelWriter excl = new ExcelWriter();
		try {
			excl.writeExcel(date, files, esiti);
			JOptionPane.showMessageDialog(getComponent(0), "File Excel creato correttamente.", "Success",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(getComponent(0), "File Excel non creato.", "Warning",
					JOptionPane.WARNING_MESSAGE);
			e.printStackTrace();
		}
	}

	public int setInfocertEnvironment() {
		int environment;

		if (infocertTest.isSelected())
			environment = 0;
		else if (infocertProCitroen.isSelected())
			environment = 1;
		else if (infocertProPeugeot.isSelected())
			environment = 2;
		else
			environment = -1;

		return environment;
	}

	// ritorna l'index xml per l'invio in conservazione (usato nel metodo send)
	// //TODO
	public ArrayList<String> getIndexXMLFatture(String fileName, int environment) {
		String metadatoData;
	/*	String metadatoDataInizio;
		String metadatoDataFine = new String();
		String[] metadati = new String[2];
		metadati = fileName.split("_"); 
		
		metadatoDataInizio = metadati[0];
		try {
		metadatoDataFine = metadati[1].substring(0, 10);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		metadatoData = metadatoDataInizio;
		if(!isDate(metadatoData) || !isDate(metadatoDataInizio) || !isDate(metadatoDataFine)) {
			JOptionPane.showMessageDialog(getComponent(0), "Un metadato non � corretto", "Error", JOptionPane.ERROR_MESSAGE);
		}*/
		
		try {
			metadatoData = fileName.substring(0, 10);
		} catch (Exception ex) {
			metadatoData = "";
			ex.printStackTrace();
		}

		if (!isDate(metadatoData)) {
			try {
				metadatoData = JOptionPane.showInputDialog(getComponent(0),
						"La data del file " + fileName + " � inesatta.\nInseriscila manualmente: ");
			} catch (HeadlessException ex) {
				ex.printStackTrace();
				errorLogSend(fileName, " c'e' stato un errore durante l'inserimento manuale della data.");
			}
			completeLog("La data del file " + fileName + " � stata modificata in " + metadatoData + ".", "DO NOTHING");
		}
		

	//	String metadatoMarca = (environment == 1) ? "Citroen" : "Peugeot";

		String metadatoMarca;
		if (environment == 0)
			metadatoMarca = "Test";
		else if (environment == 1)
			metadatoMarca = "Citroen";
		else if (environment == 2)
			metadatoMarca = "Peugeot";
		else
			metadatoMarca = "Undefined";

		ArrayList<String> returnString = new ArrayList<>();
		returnString.add("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		returnString.add("<legaldocIndex documentClass=\"gestione_ordine_vetture\" label=\"Gestione Ordini Vetture\">");
		returnString.add("<field name=\"__data_documento_dt\">" + metadatoData + "</field>");
		returnString.add("<field name=\"data_fine_dt\">" + metadatoData + "</field>");
		returnString.add("<field name=\"data_inizio_dt\">" + metadatoData + "</field>");
		returnString.add("<field name=\"marca_s\">" + metadatoMarca + "</field>");
		returnString.add("</legaldocIndex>");

		System.out.println(returnString);
		return returnString;
	}

	// ritorna param xml per l'invio in conservazione (usato nel metodo send) //TODO
	public ArrayList<String> getParamsXMLFatture(String year, String fileName, String indexHash, String pdfHash) {
		ArrayList<String> returnString = new ArrayList<>();
		returnString.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		returnString.add("<parameters>");
		returnString.add("<policy_id>" + idPolicy + "</policy_id>");
		returnString.add("<index_file>");
		returnString.add("<index_name>index.xml</index_name>");
		returnString.add("<index_hash>" + indexHash + "</index_hash>");
		returnString.add("<index_mimetype>text/xml;1.0</index_mimetype>");
		returnString.add("</index_file>");
		returnString.add("<data_file>");
		returnString.add("<data_name>" + fileName + "</data_name> ");
		returnString.add("<data_hash>" + pdfHash + "</data_hash>");
		returnString.add("<data_mimetype>application/pdf;NA</data_mimetype>");
		returnString.add("</data_file>");
		returnString.add("<path>/ordini/" + year + "</path>");
		returnString.add("<encrypted_by_owner>S</encrypted_by_owner>");
		returnString.add("</parameters>");

		return returnString;
	}

	// Usato nel metodo send()
	public void deleteFile(String fileToDelete) {
		File file = new File(fileToDelete);
		if (file.exists()) {
			try {
				file.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// logout (usato nel metodo send)
	public void logout() {
		InfoCertCore.logout(sessionId);
	}

	// passo il file, ritorna l'hash (usato nel metodo send)
	public String getHash(File file) throws Exception {

		// System.out.println("MD5 : " + toHex(Hash.MD5.checksum(file)));
		// System.out.println("SHA1 : " + toHex(Hash.SHA1.checksum(file)));
		System.out.println("SHA256 : " + toHex(Hash.SHA256.checksum(file)));
		// System.out.println("SHA512 : " + toHex(Hash.SHA512.checksum(file)));

		return toHex(Hash.SHA256.checksum(file)).toLowerCase();
	}

	// converte in HEX (usato nel metodo send)
	private static String toHex(byte[] bytes) {
		return DatatypeConverter.printHexBinary(bytes);
	}

	/***************** FINE INVIO IN CONSERVAZIONE *****************/

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MainWindow window = new MainWindow();
				try {
					window.setVisible(true);					
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(window.getComponent(0),
							"Si � verificato un errore imprevisto, contattare un admin.", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
}