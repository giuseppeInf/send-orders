import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 * Clase utilitaria que sirve para cargar distintas propiedades de los archivos de configuración
 *
 * @see PropertiesTypes
 * @author Omar del Real
 */
public class PropertiesLoader {

    // La longitud va a depender de la cantidad de objetos que estén en el enum "PropertiesTypes", de esta manera es dinámico:
    private static HashMap<PropertiesTypes, Properties> properties = new HashMap<>(PropertiesTypes.values().length);

    // Esto se ejecuta con la primera referencia a la clase, es parecido a un constructor
    // IMPORTANTE: Cada vez que añadas un properties tienes que inicializarlo aquí y en "PropertiesTypes"
    static {
        properties.put(PropertiesTypes.CONSTANTS, initProperty("./Constants.properties"));
    }

    /**
     * Devuelve el texto relacionado a una propiedad
     *
     * @param type El tipo de recurso
     * @param key  La clave del recurso
     * @return El valor relacionado con la clave de la propiedad
     */
    public static String loadProperty(PropertiesTypes type, String key) {
        return (String) properties.get(type).get(key);
    }

    /**
     * Carga el archivo de propiedades y lo devuelve
     *
     * @param URL La URL del recurso
     * @return El Archivo de propiedades correctamente inicializado
     */
    private static Properties initProperty(String URL) {
        Properties prop = new Properties();
        try {
            prop.load(PropertiesLoader.class.getResourceAsStream(URL));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return prop;
    }
}
