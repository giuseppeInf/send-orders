
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWriter {

    private static String[] columns = {"Data di elaborazione", "Nome file", "Inviato correttamente"};
    

	public void writeExcel(String[] date, File[] files, String [] esiti) throws IOException {
		 // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances for various things like DataFormat, 
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();
        // Create a Sheet
        Sheet sheet = workbook.createSheet("Riepilogo Ordini");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBoldweight((short) 2);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
        
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);
        
     // Creating cells
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        
     // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
        
     // Create Other rows and cells with employees data
        int rowNum = 1;
        for(int i = 0; i < files.length; i++) {
            Row row = sheet.createRow(rowNum++);
            
            row.createCell(0)
            	.setCellValue(date[i]);
            
            row.createCell(1)
                    .setCellValue(files[i].getName());

            row.createCell(2)
                    .setCellValue(esiti[i]);

        }
        
     // Resize all columns to fit the content size
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        FileOutputStream fileOut;
        Date now = new Date();
        SimpleDateFormat sdfLong = new SimpleDateFormat("YYYYMMddHHmmss");
		
        String dataCorrente = String.valueOf(sdfLong.format(now));
        Properties prop = loadProperties();
        
		try {
			fileOut = new FileOutputStream(prop.getProperty("path.download") + dataCorrente + ".xlsx");
			System.out.println(prop.getProperty("path.download") + dataCorrente + ".xlsx");
			workbook.write(fileOut);
		    fileOut.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// carica i parametri di Infocert
		public Properties loadProperties() {
			Properties prop = new Properties();
			InputStream input = null;

			try {

				input = new FileInputStream("config.properties");

				// load a properties file
				prop.load(input);

			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (input != null) {
					try {
						input.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			return prop;
		}
}
